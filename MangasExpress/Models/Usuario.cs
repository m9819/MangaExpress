﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MangasExpress.Models
{
    public class Usuario:BaseEntity
    {
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "El correo no puede estar vacio")]
        [MaxLength(100,ErrorMessage ="El correo no puede superar los 100 caracteres")]
        public string Correo { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "La contraseña no puede estar vacia")]
        [MaxLength(50,ErrorMessage ="La contraseña no puede superar los 50 caracteres")]
        [MinLength(8,ErrorMessage ="La contraseña debe ser de al menos 8 caracteres")]
        public string Contrasenia { get; set; }
    }
}
