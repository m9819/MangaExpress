﻿using Microsoft.EntityFrameworkCore;

namespace MangasExpress.Models
{
    public class MangaContext:DbContext
    {
        public MangaContext()
        {

        }
        public MangaContext(DbContextOptions<MangaContext> options):base(options)
        {

        }
        public DbSet<Manga> mangas { get; set; }
        public DbSet<Registrado> registrados { get; set; }
        public DbSet<Usuario> usuarios { get; set; }
    }
}
