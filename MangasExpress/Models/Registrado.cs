﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MangasExpress.Models
{
    public class Registrado:BaseEntity
    {
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "El correo es requerido")]
        public string Correo { get; set; }
        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }
    }
}
