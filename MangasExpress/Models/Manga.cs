﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MangasExpress.Models
{
    public class Manga:BaseEntity
    {
        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "El precio es requerido")]
        public double Precio { get; set; }
        [Required(ErrorMessage = "La descripcion es requerida")]
        public string Descripcion { get; set; }
        [Required(ErrorMessage = "El autor es requerido")]
        public string Autor { get; set; }
        [Required(ErrorMessage = "La marca es requerida")]
        public string Marca { get; set; }
        [Required(ErrorMessage = "La imagen es requerida")]
        public string Imagen { get; set; }
    }
}
