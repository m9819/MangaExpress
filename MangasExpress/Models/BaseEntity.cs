﻿using MangasExpress.Interfaces;
using System;

namespace MangasExpress.Models
{
    public class BaseEntity : IEntity
    {
        public int Id { get; set; }
        public bool Status { get; set; } = true;
        public DateTime CreateAt { get; set; } = DateTime.Now;
        public DateTime? UpdateAt { get; set; }
    }
}