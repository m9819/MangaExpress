﻿using MangasExpress.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace MangasExpress.Controllers
{
    public class MangasController : Controller
    {
        private MangaContext mangaContext;

        public MangasController(MangaContext _context)
        {
            mangaContext = _context;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Manga manga)
        {
            mangaContext.mangas.Add(manga);
            mangaContext.SaveChanges();
            return RedirectToAction("Index");
        }

        public IActionResult Details(int id)
        {
            var manga = mangaContext.mangas.FirstOrDefault(x => x.Id == id);
            return View(manga);
        }

        public IActionResult Index()
        {
            var mangas = mangaContext.mangas.Where(x => x.Status).ToList();
            return View(mangas);
        }
        public IActionResult Update(int id)
        {
            var manga = mangaContext.mangas.FirstOrDefault(x => x.Id == id);
            return View(manga);
        }

        [HttpPost]
        public IActionResult Update(Manga manga)
        {
            var updateManga = mangaContext.mangas.Update(manga).Entity;
            mangaContext.SaveChanges();
            return View(updateManga);
        }
    }
}