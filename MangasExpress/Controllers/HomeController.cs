﻿using MangasExpress.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PagedList;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MangasExpress.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private MangaContext mangaContext;

        public HomeController(ILogger<HomeController> logger, MangaContext _context)
        {
            _logger = logger;
            mangaContext = _context;
        }


       [HttpGet]
        public IActionResult Index(string searchBy)
        {
            var mangas = string.IsNullOrEmpty(searchBy) ? mangaContext.mangas.Where(x => x.Status).ToList() : 
                mangaContext.mangas.Where(x => x.Status && x.Nombre.Contains(searchBy)).ToList();
            return View(mangas); 
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpGet]
        public IActionResult Details(int id)
        {

            var manga = mangaContext.mangas.FirstOrDefault(x => x.Id == id);
            return PartialView("_DetailsModelPartial", manga);
        }

    }
}
