﻿using MangasExpress.Models;
using MangasExpress.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace MangasExpress.Controllers
{
    public class LoginController : Controller
    {
        UsuarioRepository repository;
        public LoginController(UsuarioRepository _reporitory)
        {
            repository = _reporitory;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(Usuario _usuario)
        {
            if (!ModelState.IsValid)
            {
                return View(_usuario);
            }
            var usuario = repository.loginUser(_usuario);
            if (usuario.Id > 0)
            {
                HttpContext.Session.SetString("Id", usuario.Id.ToString());
                return RedirectToAction("Index", "Home");
            }
           
            return View();
        }
        public IActionResult Create()
        {
            
            return View();
        }
        [HttpPost]
        public IActionResult Create(Usuario _usuario)
        {
            if (!ModelState.IsValid)
            {
                return View(_usuario);
            }
            var usuario = repository.createUser(_usuario);
            if (usuario.Id > 0)
            {
                HttpContext.Session.SetString("Id", usuario.Id.ToString());
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
    }
}
