﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MangasExpress.Configuration
{
    public class MyAppSettings
    {
        public const string SectionName = "MyAppSettings";
        public string privateKey { get; set; }
        public string publicKey { get; set; }
    }
}
