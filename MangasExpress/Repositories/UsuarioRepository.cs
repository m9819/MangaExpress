﻿using MangasExpress.Configuration;
using MangasExpress.Models;
using MangasExpress.Security;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MangasExpress.Repositories
{
    public class UsuarioRepository
    {
        private MangaContext context;
        private IOptions<MyAppSettings> options;
        public UsuarioRepository(MangaContext _context, IOptions<MyAppSettings> _options)
        {
            context = _context;
            options = _options;
        }

        public Usuario createUser(Usuario _usuario)
        {
            try
            {
                EncryptDecrypt encrypt = new EncryptDecrypt(options);
                _usuario.Contrasenia = encrypt.Encrypt(_usuario.Contrasenia);
                _usuario.CreateAt = DateTime.Now;
                _usuario.Status = true;
                context.usuarios.Add(_usuario);
                context.SaveChanges();
            }
            catch
            {
                _usuario = new Usuario();
            }
            return _usuario;
        }

        public Usuario loginUser(Usuario _usuario)
        {
            Usuario usuario;
            try
            {
                EncryptDecrypt encrypt = new EncryptDecrypt(options);
                _usuario.Contrasenia = encrypt.Encrypt(_usuario.Contrasenia);
                usuario = context.usuarios.FirstOrDefault(x => x.Correo == _usuario.Correo && x.Contrasenia == _usuario.Contrasenia);
            }catch{
                usuario = new Usuario();
            }
            return (usuario != null) ? usuario : new Usuario();
        }
        public Usuario updateUser(Usuario _usuario)
        {
            try
            {
                EncryptDecrypt encrypt = new EncryptDecrypt(options);
                _usuario.Contrasenia = encrypt.Encrypt(_usuario.Contrasenia);
                _usuario.UpdateAt = DateTime.Now;
                context.usuarios.Update(_usuario);
                context.SaveChanges();
            }
            catch
            {
                _usuario = new Usuario();
            }
            return _usuario;
        }
    }
}
